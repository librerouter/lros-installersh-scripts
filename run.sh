set -x
echo "running installer"

cp utils.lua /usr/lib/lua/lime/utils.lua
cp safe-upgrade /usr/sbin/safe-upgrade
chmod +x /usr/sbin/safe-upgrade

tar cvfz /tmp/archive.tar.gz /etc/config/lime-node /etc/config/lime-community /etc/config/libremap /etc/config/location /etc/dropbear/ /root/.ssh/known_hosts /etc/shadow

safe-upgrade upgrade --preserve-archive /tmp/archive.tar.gz firmware.bin
